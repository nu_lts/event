<?php
/**
 * @file
 * event.features.inc
 */

/**
 * Implements hook_node_info().
 */
function event_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('Used to create two calendar pages (one general, one DMDS) which can be filtered by type,  topic, and date. Used to create single node pages and the Upcoming Event widget.'),
      'has_title' => '1',
      'title_label' => t('Header'),
      'help' => '',
    ),
  );
  return $items;
}
