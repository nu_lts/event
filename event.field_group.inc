<?php
/**
 * @file
 * event.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function event_field_group_info() {
  $export = array();

  $field_group = new stdClass;
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ev_detail_info_inside|node|event|default';
  $field_group->group_name = 'group_ev_detail_info_inside';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_ev_detail_info';
  $field_group->data = array(
    'label' => 'Detail info inside',
    'weight' => '7',
    'children' => array(
      0 => 'field_ev_date',
      1 => 'field_ev_area',
      2 => 'field_ev_type',
      3 => 'field_ev_topic',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Detail info inside',
      'instance_settings' => array(
        'classes' => 'events-detail-info-inside',
        'description' => '',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_ev_detail_info_inside|node|event|default'] = $field_group;

  $field_group = new stdClass;
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ev_detail_info|node|event|default';
  $field_group->group_name = 'group_ev_detail_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Detail info',
    'weight' => '1',
    'children' => array(
      0 => 'field_ev_image_large',
      1 => 'field_ev_image_small',
      2 => 'group_ev_detail_info_inside',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Detail info',
      'instance_settings' => array(
        'classes' => 'events-detail-info',
        'description' => '',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_ev_detail_info|node|event|default'] = $field_group;

  return $export;
}
